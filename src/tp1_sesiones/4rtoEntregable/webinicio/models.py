from django.db import models

# Create your models here.


class AuditoriaFecha(models.Model):
    f_creacion = models.DateTimeField(auto_now_add=True)
    f_actualizacion = models.DateTimeField(auto_now=True)
    class Meta:
        abstract = True
    
class Persona(AuditoriaFecha):
    nombre=models.CharField(max_length=255)
    apellido=models.CharField(max_length=255)
    email=models.CharField(max_length=255)
    f_nacimiento = models.DateField()
    usuario = models.CharField(max_length=255)
    clave = models.CharField(max_length=255)
    
    def __str__(self) :
        return "Persona:  {} ".format(self.usuario)
    

class Producto(AuditoriaFecha):
    nombre_prod = models.CharField(max_length=255)
    descripcion_prod=models.CharField(max_length=255)
    categoria_prod=models.CharField(max_length=255)
    cantidad_prod=models.IntegerField()
    valor = models.FloatField()
    
    
    def __str__(self) :
        return "Producto:  {} $: {} #: {}".format(self.nombre_prod,self.valor,self.cantidad_prod)


