from django import forms
from .models import Persona
from django.contrib.auth.forms import AuthenticationForm

class PersonaForm(forms.ModelForm):
    class Meta:
        model = Persona
        fields = '__all__'
        
        
       

class LoginForm(forms.Form):
    usuario = forms.CharField(required=True,widget=forms.TextInput())
    clave = forms.CharField(required=True,widget=forms.PasswordInput())
    
    class Meta:
        model = Persona