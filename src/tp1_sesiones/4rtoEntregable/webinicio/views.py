from django.shortcuts import render, redirect
from django.http import HttpResponse
from .forms import PersonaForm
from django.contrib import messages
from django.contrib.auth import authenticate, login
from .forms import LoginForm
from .models import Persona
from .models import Producto 
from django.shortcuts import render, get_object_or_404

# Create your views here.
def inicio(request):
     #return HttpResponse("estoy en la pagina de inicio")
      #return render(request='inicio.html')
       return render(request, 'paginas/inicio.html')
        
 
def registro(request):
       formulario=PersonaForm(request.POST or None)
       if request.method == 'POST':
        if formulario.is_valid():
              formulario.save()
              messages.success(request, 'Se registró con éxito')
              return redirect('login')
       
       return render(request, 'paginas/registro.html', {'formulario':formulario})
 
 
def login(request):
    mensaje={"estado": "", "mensaje":"Bienvenido"    }
    usuariodb = None
   
    if request.method == 'POST':
        form = LoginForm(data=request.POST)
        usuario = request.POST.get('usuario','')
        clave = request.POST.get('clave','')
        print(usuario,clave)
        usuariodb = Persona.objects.filter(usuario=usuario,clave=clave)
        if len(usuariodb)>0:
            return redirect("marketplace") #Si el usuario y clave son correctos redirige aquí
        else:
            mensaje['form_login']=form
            return render(request,'paginas/login.html', {'formulario':form}) #Si son incorrectos se queda en login
    else:
        form = LoginForm(data=request.GET)
        mensaje['form_login']=form

        
    return render(request, 'paginas/login.html',{'formulario':form})

     
     
     
     
def marketplace(request):
       return render(request, 'paginas/marketplace.html')
             
             
def marketplace_cat(request, categoria_prod):
    productos = Producto.objects.filter(categoria_prod=categoria_prod)
    return render(request, 'paginas/marketplace_cat.html', {'categoria': categoria_prod, 'productos': productos})
             
def detalle(request, producto_id):
    #producto = Producto.objects.get(id=producto_id)
    producto = get_object_or_404(Producto, id=producto_id)
    return render(request, 'paginas/detalle.html', {'producto': producto})

