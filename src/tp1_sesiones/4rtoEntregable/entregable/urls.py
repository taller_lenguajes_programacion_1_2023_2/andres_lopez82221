"""
URL configuration for entregable project.

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/4.2/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path
from django.urls import include
from webinicio import urls 
from webinicio.views import inicio, registro, login, marketplace_cat, marketplace, detalle



urlpatterns = [
    path('admin/', admin.site.urls),
  # path('', include(webinicio.urls)),
    path('',inicio,name="inicio"),
    path('registro',registro,name="registro"),
    path('login',login,name="login"),
    path('marketplace',marketplace,name="marketplace"),
    path('marketplace_cat/<str:categoria_prod>/',marketplace_cat,name="marketplace_cat"),
    path('detalle/<int:producto_id>/',detalle,name="detalle"),
]
