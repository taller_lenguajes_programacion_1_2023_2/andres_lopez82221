from Tienda import Tienda
from Parcial import Excel_entregable
from conexion_entregable import Conexion_entregable

class Cliente:
    def __init__(self, id_cliente, nombre, correo):
        self.id_cliente = id_cliente
        self.nombre = nombre
        self.correo = correo

    def get_id_cliente(self):
        return self.id_cliente

    def set_id_cliente(self, nuevo_id):
        self.id_cliente = nuevo_id

    def get_nombre(self):
        return self.nombre

    def set_nombre(self, nuevo_nombre):
        self.nombre = nuevo_nombre

    def get_correo(self):
        return self.correo

    def set_correo(self, nuevo_correo):
        self.correo = nuevo_correo

    def __str__(self):
        return f"ID Cliente: {self.id_cliente}, Nombre: {self.nombre}, Correo: {self.correo}"
