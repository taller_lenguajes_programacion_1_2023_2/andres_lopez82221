from conexion_entregable import Conexion_entregable

class Tienda:
    def __init__(self, direccion, nombre):
        self.direccion = direccion
        self.nombre = nombre

    def get_id_tienda(self):
        return self.direccion

    def set_id_tienda(self, nuevo_direccion):
        self.direccion = nuevo_direccion

    def get_nombre(self):
        return self.nombre

    def set_nombre(self, nuevo_nombre):
        self.nombre = nuevo_nombre

    def __str__(self):
        return f"ID Tienda: {self.direccion}, Nombre: {self.nombre}"
