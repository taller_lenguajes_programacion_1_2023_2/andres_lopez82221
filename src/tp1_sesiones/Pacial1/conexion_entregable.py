
import sqlite3
import json

class Conexion_parcial:
    def __init__(self, db_nombre):
        self.conn = sqlite3.connect(db_nombre)

    def insertar(self, tabla, valores):
        cursor = self.conn.cursor()
        cursor.execute(f"INSERT INTO {tabla} VALUES {valores}")
        self.conn.commit()

    def eliminar(self, tabla, condicion):
        cursor = self.conn.cursor()
        cursor.execute(f"DELETE FROM {tabla} WHERE {condicion}")
        self.conn.commit()

    def actualizar(self, tabla, valores, condicion):
        cursor = self.conn.cursor()
        cursor.execute(f"UPDATE {tabla} SET {valores} WHERE {condicion}")
        self.conn.commit()

    def seleccionar(self, tabla, condicion="1"):
        cursor = self.conn.cursor()
        cursor.execute(f"SELECT * FROM {tabla} WHERE {condicion}")
        return cursor.fetchall()

    def ejecutar_queries_desde_json(self, archivo_json):
        with open(archivo_json, 'r') as archivo:
            queries = json.load(archivo)

        cursor = self.conn.cursor()
        for query in queries:
            cursor.execute(query)
        self.conn.commit()
