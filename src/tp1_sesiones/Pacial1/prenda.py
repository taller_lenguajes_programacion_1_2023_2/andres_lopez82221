from conexion_entregable import Conexion_entregable

class Prenda:
    def __init__(self, id_prenda, nombre, precio):
        self.id_prenda = id_prenda
        self.nombre = nombre
        self.precio = precio

    def get_id_prenda(self):
        return self.id_prenda

    def set_id_prenda(self, nuevo_id):
        self.id_prenda = nuevo_id

    def get_nombre(self):
        return self.nombre

    def set_nombre(self, nuevo_nombre):
        self.nombre = nuevo_nombre

    def get_precio(self):
        return self.precio

    def set_precio(self, nuevo_precio):
        self.precio = nuevo_precio

    def __str__(self):
        return f"ID Prenda: {self.id_prenda}, Nombre: {self.nombre}, Precio: {self.precio}"
