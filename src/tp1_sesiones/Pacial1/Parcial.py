import pandas as pd

class Excel_parcial:
    def __init__(self,nombre_xlsx= "") -> None:
        self.___nombre_xlsx = nombre_xlsx
       
        self.__ruta_xlsx = "C:\Users\Andres\Desktop\Documentos\Universidad\andres_lopez82221\src\tp1_sesiones\Pacial1\static\xlsx\parcial_2.xlsx".format(self.___nombre_xlsx)
        
    def leer_xlsx(self, nom_hoja=""):
        if nom_hoja == "":
            df = pd.read_excel(self.__ruta_xlsx)
        else:
            df = pd.read_excel(self.__ruta_xlsx,sheet_name=nom_hoja)
        return df
