from conexion_entregable import Conexion_entregable

class Compra:
    def __init__(self, id_compra, id_cliente, id_prenda, cantidad):
        self.id_compra = id_compra
        self.id_cliente = id_cliente
        self.id_prenda = id_prenda
        self.cantidad = cantidad

    def get_id_compra(self):
        return self.id_compra

    def set_id_compra(self, nuevo_id):
        self.id_compra = nuevo_id

    def get_id_cliente(self):
        return self.id_cliente

    def set_id_cliente(self, nuevo_id):
        self.id_cliente = nuevo_id

    def get_id_prenda(self):
        return self.id_prenda

    def set_id_prenda(self, nuevo_id):
        self.id_prenda = nuevo_id

    def get_cantidad(self):
        return self.cantidad

    def set_cantidad(self, nueva_cantidad):
        self.cantidad = nueva_cantidad

    def __str__(self):
        return f"ID Compra: {self.id_compra}, ID Cliente: {self.id_cliente}, ID Prenda: {self.id_prenda}, Cantidad: {self.cantidad}"
