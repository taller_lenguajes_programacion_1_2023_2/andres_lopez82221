from Conexion import Conexion

class Entrenador(Conexion):
    def __init__(self, entNombre="", region=""):
        
        Conexion.__init__(self)
        self.entNombre = entNombre
        self.region = region
        self.CrearDB()
        
    #CRUD
    
    def CrearDB(self):
        self.CrearTabla("Trainers","Entrenador")
        
    def LeerDB(self):
        self.listaDeDatos = self.VerTodo("Trainers")
        
    def EditarDB(self, nuevoDato, columna, id):
        if self.ActualizarDatos(nuevoDato,"Trainers",columna,id):
            return True
        else:
            return False
        
    def EliminarDB(self, id):
        if self.EliminarDatos("Trainers", id):
            return True
        else:
            return False
        
    def InsertarDB(self):
        datos = [
            (f"{self.entNombre}",f"{self.region}")
        ]
        self.InsertarDatos("Trainers",2,datos)