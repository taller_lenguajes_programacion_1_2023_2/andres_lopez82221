from Conexion import Conexion

class Pokemon(Conexion):
    def __init__(self, PokemonsNombre="", traID=0):
        
        Conexion.__init__(self)
        self.PokemonsNombre = PokemonsNombre
        self.traID = traID
        self.CrearDB()
        
    
    def CrearDB(self):
        self.CrearTabla("Pokemons","Pokemon")
        
    def LeerDB(self):
        self.listaDeDatos = self.VerTodo("Pokemons")
        
    def EditarDB(self, nuevoDato, columna, id):
        if self.ActualizarDatos(nuevoDato,"Pokemons",columna,id):
            return True
        else:
            return False
        
    def EliminarDB(self, id):
        if self.EliminarDatos("Pokemons", id):
            return True
        else:
            return False
        
    def InsertarDB(self):
        datos = [
            (f"{self.PokemonsNombre}",f"{self.traID}")
        ]
        self.InsertarDatos("Pokemons",2,datos)
        