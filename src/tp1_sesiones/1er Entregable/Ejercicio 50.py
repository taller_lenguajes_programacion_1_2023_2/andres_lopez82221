# Escribir un programa que transforme un archivo CSV en un diccionario donde las claves sean los encabezados de las columnas y los valores sean listas de datos para cada columna

import pandas as pd


adlq_archivo_csv = "C:\\Users\\Andres\\Desktop\\Documentos\\Universidad\\andres_lopez82221\\src\\static\\1er Entregable\\datos.csv"
adlq_df = pd.read_csv(adlq_archivo_csv)


adlq_diccionario = adlq_df.to_dict(orient='list')


print(adlq_diccionario)