#escribir un programa que lea un archivo excel y aplique una funcion personalizada a una columna numeria especifica

import pandas as pd


adlq_archivo_excel = "C:\\Users\\Andres\\Desktop\\Documentos\\Universidad\\andres_lopez82221\\src\\static\\1er Entregable\\datos3.xlsx" 


df = pd.read_excel(adlq_archivo_excel)


def adlq_funcion_personalizada(valor):
    
    return valor * 2


df['primero'] = df['primero'].apply(adlq_funcion_personalizada)


adlq_nuevo_archivo_excel = 'datos_modificados.xlsx'
df.to_excel(adlq_nuevo_archivo_excel, index=False)

print(f"Se ha aplicado la función personalizada y se ha guardado en '{adlq_nuevo_archivo_excel}'.")
