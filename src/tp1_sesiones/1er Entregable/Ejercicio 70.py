#Simular un archivo Excel, escribir un programa que encuentre el valor maximo y minimo de una columna munerica especifica

import pandas as pd


adlq_archivo_excel = "C:\\Users\\Andres\\Desktop\\Documentos\\Universidad\\andres_lopez82221\\src\\static\\1er Entregable\\datos3.xlsx" 


adlq_columna_a_analizar = "tercero"


df = pd.read_excel(adlq_archivo_excel)


adlq_valor_maximo = df[adlq_columna_a_analizar].max()
adlq_valor_minimo = df[adlq_columna_a_analizar].min()

print(f"Valor máximo en la columna {adlq_columna_a_analizar}: {adlq_valor_maximo}")
print(f"Valor mínimo en la columna {adlq_columna_a_analizar}: {adlq_valor_minimo}")