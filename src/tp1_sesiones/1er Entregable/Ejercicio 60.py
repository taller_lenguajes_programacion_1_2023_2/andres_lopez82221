#Escribir un programa que compare dos archivos CSV y muestre las diferencias entre ellos

import pandas as pd


adlq_archivo1 = "C:\\Users\\Andres\\Desktop\\Documentos\\Universidad\\andres_lopez82221\\src\\static\\1er Entregable\\datos.csv"  
adlq_archivo2 = "C:\\Users\\Andres\\Desktop\\Documentos\\Universidad\\andres_lopez82221\\src\\static\\1er Entregable\\datos2.csv"  

adlq_df1 = pd.read_csv(adlq_archivo1)
adlq_df2 = pd.read_csv(adlq_archivo2)


adlq_diferencias = adlq_df1.compare(adlq_df2)


print("Diferencias entre archivo1.csv y archivo2.csv:")
print(adlq_diferencias)