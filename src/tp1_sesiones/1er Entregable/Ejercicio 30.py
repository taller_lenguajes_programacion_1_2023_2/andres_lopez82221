# Dada una lista escribir un programa que devuelva una lista con los elementos inversos

def elementos_inversos(adlq_lista):
    
    adlq_lista_inversa = adlq_lista[::-1]
    return adlq_lista_inversa


adlq_mi_lista = [1, 2, 3, 4, 5, 6]


adlq_lista_inversa = elementos_inversos(adlq_mi_lista)


print("Lista Original:", adlq_mi_lista)
print("Lista Inversa:", adlq_lista_inversa)