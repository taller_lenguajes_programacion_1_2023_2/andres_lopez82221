#Escribe un programa que tome un numero entero n y devuelva su factorial en python

def adlq_calcular_factorial(adlq_numero):
    
    adlq_factorial = 1

    
    if adlq_numero < 0:
        return "No se puede calcular el factorial de un número negativo."
    elif adlq_numero == 0:
        return "El factorial de 0 es 1."

    
    for i in range(1, adlq_numero + 1):
        adlq_factorial *= i

    return f"El factorial de {adlq_numero} es {adlq_factorial}"


try:
    adlq_numero = int(input("Ingrese un número entero: "))
    adlq_resultado = adlq_calcular_factorial(adlq_numero)
    print(adlq_resultado)
except ValueError:
    print("Debe ingresar un número entero válido.")