# Simula un archivo html, escribir un programa que convierta todas las tablas en el archivo a archivos excel separados

import pandas as pd
from bs4 import BeautifulSoup
import os


adlq_archivo_html = "C:\\Users\\Andres\\Desktop\\Documentos\\Universidad\\andres_lopez82221\\src\\static\\1er Entregable\\ejemplo.html"


with open(adlq_archivo_html, 'r', encoding='utf-8') as archivo:
    adlq_soup = BeautifulSoup(archivo, 'html.parser')
    adlq_tablas = adlq_soup.find_all('table')


adlq_directorio_salida = 'tablas_excel'
if not os.path.exists(adlq_directorio_salida):
    os.mkdir(adlq_directorio_salida)


for i, tabla in enumerate(adlq_tablas, 1):
    df = pd.read_html(str(tabla), header=0)[0]  # Lee la tabla y crea un DataFrame
    adlq_archivo_excel = os.path.join(adlq_directorio_salida, f'tabla_{i}.xlsx')
    df.to_excel(adlq_archivo_excel, index=False)

print(f"Se han convertido {len(adlq_tablas)} tablas a archivos Excel en el directorio '{adlq_directorio_salida}'.")
