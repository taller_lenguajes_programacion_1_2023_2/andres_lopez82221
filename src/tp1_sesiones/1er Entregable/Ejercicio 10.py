
#Escribir un programa que tome una cadena s y devuelva el primer y ultimo digito

def adlq_obtener_primer_y_ultimo_digito(adlq_cadena):
   
    if len(adlq_cadena) == 0:
        return "La cadena está vacía, no hay dígitos para obtener."

    
    adlq_primer_digito = adlq_cadena[0]

    
    adlq_ultimo_digito = adlq_cadena[-1]

    return f"El primer dígito es: {adlq_primer_digito}, y el último dígito es: {adlq_ultimo_digito}"


adlq_cadena = "Buenos dias"


adlq_resultado = adlq_obtener_primer_y_ultimo_digito(adlq_cadena)
print(adlq_resultado)