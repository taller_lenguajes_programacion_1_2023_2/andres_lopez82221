# Dado dos diccionarios, escribir un programa que verifique si tiene claves en comun 

def adlq_tienen_claves_en_comun(adlq_diccionario1, adlq_diccionario2):
    
    adlq_claves1 = set(adlq_diccionario1.keys())
    adlq_claves2 = set(adlq_diccionario2.keys())
    
    
    if adlq_claves1.intersection(adlq_claves2):
        return True
    else:
        return False


adlq_diccionario1 = {'a': 1, 'b': 2, 'f': 3}
adlq_diccionario2 = {'c': 4, 'd': 5, 'e': 6}


if adlq_tienen_claves_en_comun(adlq_diccionario1, adlq_diccionario2):
    print("Los diccionarios tienen claves en común.")
else:
    print("Los diccionarios no tienen claves en común.")