import tkinter as tk 
from tkinter import ttk
from model.entrenador_dao import crear_tabla, borrar_tabla
from model.entrenador_dao import Entrenador, guardar


def barra_menu(root):
    barra_menu=tk.Menu(root)
    root.config(menu=barra_menu,width=300, height=300)

    menu_inicio = tk.Menu(barra_menu, tearoff=0)
    barra_menu.add_cascade(label="Inicio", menu=menu_inicio)

    menu_inicio.add_command(label="Crear Registor DB", command=crear_tabla)
    menu_inicio.add_command(label="Eliminar REgistor DB", command=borrar_tabla)
    menu_inicio.add_command(label="salir", command=root.destroy)

    barra_menu.add_cascade(label="Consultas", menu=menu_inicio)
    barra_menu.add_cascade(label="Ayuda", menu=menu_inicio)


class Frame(tk.Frame):
    def __init__(self, root = None):
        super().__init__(root,width=480, height=320)
        self.root = root
        self.pack()
       # self.config(bg="green")

        self.campos_entrenador()
        self.desabilitar_campos()
        self.tabla_entrenadores()


    def campos_entrenador(self):
        #label de cada campo
        
        self.label_nombre = tk.Label(self, text = "Nombre: ")
        self.label_nombre.config(font= ("Arial", 12,"bold"))
        self.label_nombre.grid(row=0,column=0, padx=10,pady=10) 
        
        self.label_region = tk.Label(self, text = "ID entrenador: ")
        self.label_region.config(font= ("Arial", 12,"bold"))
        self.label_region.grid(row=1,column=0, padx=10,pady=10) 

        #Entrys de cada campo 
        
        self.mi_nombre = tk.StringVar()
        self.entry_nombre = tk.Entry(self, textvariable=self.mi_nombre)
        self.entry_nombre.config(width=50,font= ("Arial", 12))
        self.entry_nombre.grid(row=0,column=1, padx=10,pady=10, columnspan= 2)

        self.mi_region = tk.StringVar()
        self.entry_region = tk.Entry(self, textvariable=self.mi_region)
        self.entry_region.config(width=50,font= ("Arial", 12))
        self.entry_region.grid(row=1,column=1, padx=10,pady=10, columnspan= 2)

        #botones 

        self.boton_nuevo = tk.Button(self, text="Nuevo", command=self.habilitar_campos)
        self.boton_nuevo.config(width=20, font=("Arail",12,"bold"),fg= "#DAD5D6",bg="#158645",cursor="hand2", activebackground="#35BD6F")
        self.boton_nuevo.grid(row=4,column=0, padx=10,pady=10)

        self.boton_guardar = tk.Button(self, text="Guardar", command=self.guardar_datos)
        self.boton_guardar.config(width=20, font=("Arail",12,"bold"),fg= "#DAD5D6",bg="#1658A2",cursor="hand2", activebackground="#3586DF")
        self.boton_guardar.grid(row=4,column=1, padx=10,pady=10)

        self.boton_cancelar = tk.Button(self, text="Cancelar", command=self.desabilitar_campos)
        self.boton_cancelar.config(width=20, font=("Arail",12,"bold"),fg= "#DAD5D6",bg="#BD152E",cursor="hand2", activebackground="#E15370")
        self.boton_cancelar.grid(row=4,column=2, padx=10,pady=10)

    def habilitar_campos(self):
        self.entry_nombre.config(state="normal")
        self.entry_region.config(state="normal")

        self.boton_guardar.config(state="normal")
        self.boton_cancelar.config(state="normal")

    def desabilitar_campos(self):
        
        self.mi_nombre.set('')
        self.mi_region.set('')

        self.entry_nombre.config(state="disabled")
        self.entry_region.config(state="disabled")

        self.boton_guardar.config(state="disabled")
        self.boton_cancelar.config(state="disabled")


    def guardar_datos(self):

        entrenador = Entrenador(
            self.mi_nombre.get(),
            self.mi_region.get(),
        ) 

        guardar(entrenador)
        

        self.desabilitar_campos()


   
    def tabla_entrenadores(self):

        #self.lista_entrenadores = listar()     
        
        self.tabla = ttk.Treeview(self,columns=("ID","Nombre","Region"))
        self.tabla.grid(row=5, column=0,columnspan=4)

        self.tabla.heading("#0",text="ID")
        self.tabla.heading("#1",text="Nombre")
        self.tabla.heading("#2",text="Region")

        
        self.tabla.insert("",0,text="1", values=("Ash","Kanto"))

        self.boton_editar = tk.Button(self, text="Editar")
        self.boton_editar.config(width=20, font=("Arail",12,"bold"),fg= "#DAD5D6",bg="#158645",cursor="hand2", activebackground="#35BD6F")
        self.boton_editar.grid(row=6,column=0, padx=10,pady=10)

        self.boton_eliminar = tk.Button(self, text="Eliminar")
        self.boton_eliminar.config(width=20, font=("Arail",12,"bold"),fg= "#DAD5D6",bg="#BD152E",cursor="hand2", activebackground="#E15370")
        self.boton_eliminar.grid(row=6,column=1, padx=10,pady=10)
        
class Frame2(tk.Frame):
    def __init__(self, root = None):
        super().__init__(root,width=480, height=320)
        self.root = root
        self.pack()
       # self.config(bg="green")

        self.campos_entrenador()
        self.desabilitar_campos()
        self.tabla_entrenadores()


    def campos_entrenador(self):
        #label de cada campo
        
        self.label_nombre = tk.Label(self, text = "Nombre: ")
        self.label_nombre.config(font= ("Arial", 12,"bold"))
        self.label_nombre.grid(row=0,column=0, padx=10,pady=10) 
        
        self.label_region = tk.Label(self, text = "Region: ")
        self.label_region.config(font= ("Arial", 12,"bold"))
        self.label_region.grid(row=1,column=0, padx=10,pady=10) 

        #Entrys de cada campo 
        
        self.mi_nombre = tk.StringVar()
        self.entry_nombre = tk.Entry(self, textvariable=self.mi_nombre)
        self.entry_nombre.config(width=50,font= ("Arial", 12))
        self.entry_nombre.grid(row=0,column=1, padx=10,pady=10, columnspan= 2)

        self.mi_region = tk.StringVar()
        self.entry_region = tk.Entry(self, textvariable=self.mi_region)
        self.entry_region.config(width=50,font= ("Arial", 12))
        self.entry_region.grid(row=1,column=1, padx=10,pady=10, columnspan= 2)

        #botones 

        self.boton_nuevo = tk.Button(self, text="Nuevo", command=self.habilitar_campos)
        self.boton_nuevo.config(width=20, font=("Arail",12,"bold"),fg= "#DAD5D6",bg="#158645",cursor="hand2", activebackground="#35BD6F")
        self.boton_nuevo.grid(row=4,column=0, padx=10,pady=10)

        self.boton_guardar = tk.Button(self, text="Guardar", command=self.guardar_datos)
        self.boton_guardar.config(width=20, font=("Arail",12,"bold"),fg= "#DAD5D6",bg="#1658A2",cursor="hand2", activebackground="#3586DF")
        self.boton_guardar.grid(row=4,column=1, padx=10,pady=10)

        self.boton_cancelar = tk.Button(self, text="Cancelar", command=self.desabilitar_campos)
        self.boton_cancelar.config(width=20, font=("Arail",12,"bold"),fg= "#DAD5D6",bg="#BD152E",cursor="hand2", activebackground="#E15370")
        self.boton_cancelar.grid(row=4,column=2, padx=10,pady=10)

    def habilitar_campos(self):
        self.entry_nombre.config(state="normal")
        self.entry_region.config(state="normal")

        self.boton_guardar.config(state="normal")
        self.boton_cancelar.config(state="normal")

    def desabilitar_campos(self):
        
        self.mi_nombre.set('')
        self.mi_region.set('')

        self.entry_nombre.config(state="disabled")
        self.entry_region.config(state="disabled")

        self.boton_guardar.config(state="disabled")
        self.boton_cancelar.config(state="disabled")


    def guardar_datos(self):

        entrenador = Entrenador(
            self.mi_nombre.get(),
            self.mi_region.get(),
        ) 

        guardar(entrenador)
        

        self.desabilitar_campos()


   
    def tabla_entrenadores(self):

        #self.lista_entrenadores = listar()     
        
        self.tabla = ttk.Treeview(self,columns=("ID","Nombre","Region"))
        self.tabla.grid(row=5, column=0,columnspan=4)

        self.tabla.heading("#0",text="ID")
        self.tabla.heading("#1",text="Nombre")
        self.tabla.heading("#2",text="Region")

        
        self.tabla.insert("",0,text="1", values=("Ash","Kanto"))

        self.boton_editar = tk.Button(self, text="Editar")
        self.boton_editar.config(width=20, font=("Arail",12,"bold"),fg= "#DAD5D6",bg="#158645",cursor="hand2", activebackground="#35BD6F")
        self.boton_editar.grid(row=6,column=0, padx=10,pady=10)

        self.boton_eliminar = tk.Button(self, text="Eliminar")
        self.boton_eliminar.config(width=20, font=("Arail",12,"bold"),fg= "#DAD5D6",bg="#BD152E",cursor="hand2", activebackground="#E15370")
        self.boton_eliminar.grid(row=6,column=1, padx=10,pady=10)