from .conexion_db import ConexionDB
from tkinter import messagebox

def crear_tabla():
    conexion = ConexionDB()
    cursor = conexion.cursor()

    sql = '''
    CREATE TABLE entrenador(
    id_entrenador INTEGER,
    nombre VARCHAR(50),
    region VARCHAR(50),
    PRIMARY KEY(id_entrenador AUTOINCREMENT)
    ) 
 '''
    try:
        cursor.execute(sql)
        titulo = "Crear Registro"
        mensaje = "Se creo la tabla en la base de datos"
        messagebox.showinfo(titulo,mensaje)

    except:
        titulo = "Crear Registro"
        mensaje = "La tabla ya esta creada"
        messagebox.showwarning(titulo,mensaje)   


def borrar_tabla():
    conexion = ConexionDB()
    cursor = conexion.cursor()

    sql = "DROP TABLE entrenador"

    try:
        cursor.execute(sql)
        conexion.cerrar()
        titulo = "Borrar Registro"
        mensaje = "Se borro la tabla de la base de datos"
        messagebox.showinfo(titulo,mensaje)

    except:    
        titulo = "Borrar Registro"
        mensaje = "No hay tabla para borrar"
        messagebox.showerror(titulo,mensaje)



class Entrenador:
    def __init__(self,nombre="",region=""):
        self.nombre = nombre
        self.region = region

    def __str__(self):
        return f"Entrenador[{self.nombre},{self.region}]"



def guardar(entrenador):
    conexion = ConexionDB()
    cursor = conexion.cursor()
    
    sql = f"""INSERT INTO entrenador (nombre, region)
    VALUES('{entrenador.nombre}','{entrenador.region}')"""

    try:
        cursor.execute(sql)
        conexion.cerrar()
    except:
        titulo =  "Conexion al registro" 
        mensaje = "La tabla entrendor no esta creada"
        messagebox.showerror(titulo,mensaje)


def listar():
    conexion = ConexionDB()
    cursor = conexion.cursor()

    lista_entrenador = []
    sql = "SELCT * FROM  entrenador"

    try:
        cursor.execute(sql)
        lista_entrenador = cursor.fetchall()
        conexion.cerrar()
    except:
        titulo =  "Conexion al registro" 
        mensaje = "La tabla entrendor no esta creada"
        messagebox.showerror(titulo,mensaje)

    return lista_entrenador   