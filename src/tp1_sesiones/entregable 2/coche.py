from conexion_entregable import Conexion_entregable

class Coche(Conexion_entregable):
    def __init__(self,id = 0,marca="",modelo="",ano_fabricacion="",combustible="",km=0,color="",num_puertas=0):

        self.__id = id
        self.__marca=marca
        self.__modelo=modelo
        self.__ano_fabricacion=ano_fabricacion
        self.__combustible=combustible
        self.__km=km
        self.__color=color
        self.__num_puertas=num_puertas
        super().__init__()
        self.__crear_Coche()
    
    def __crear_Coche(self):
        nom_clase = "coches"
        if self.crear_tabla(nom_tabla="{}".format(nom_clase),nom_json="nom_coche"):
            print("Tabla {} creada !!!".format(nom_clase))

    @property
    def _id(self):
        return self.__id

    @_id.setter
    def _id(self, value):
        self.__id = value

    @property
    def _marca(self):
        return self.__marca

    @_marca.setter
    def _marca(self, value):
        self.__marca = value

    @property
    def _modelo(self):
        return self.__modelo

    @_modelo.setter
    def _modelo(self, value):
        self.__modelo = value

    @property
    def _ano_fabricacion(self):
        return self.__ano_fabricacion

    @_ano_fabricacion.setter
    def _ano_fabricacion(self, value):
        self.__ano_fabricacion = value

    @property
    def _combustible(self):
        return self.__combustible

    @_combustible.setter
    def _combustible(self, value):
        self.__combustible = value

    @property
    def _km(self):
        return self.__km

    @_km.setter
    def _km(self, value):
        self.__km = value

    @property
    def _color(self):
        return self.__color

    @_color.setter
    def _color(self, value):
        self.__color = value

    @property
    def _num_puertas(self):
        return self.__num_puertas

    @_num_puertas.setter
    def _num_puertas(self, value):
        self.__num_puertas = value

        
    def __str__(self) :
        return f"Coches {self.__id} {self.__marca}{self.__modelo}{self.__ano_fabricacion} {self.__combustible}{self.__km}{self.__color}{self._num_puertas}"
