import pandas as pd

class Excel_entregable:
    def __init__(self,nombre_xlsx= "") -> None:
        self.___nombre_xlsx = nombre_xlsx
       
        self.__ruta_xlsx = "./src/tp1_sesiones/entregable 2/static/xlsx/{}".format(self.___nombre_xlsx)
        
    def leer_xlsx(self, nom_hoja=""):
        if nom_hoja == "":
            df = pd.read_excel(self.__ruta_xlsx)
        else:
            df = pd.read_excel(self.__ruta_xlsx,sheet_name=nom_hoja)
        return df
